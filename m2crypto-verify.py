from M2Crypto import RSA, EVP
import hashlib
import base64


def payload(secret, userid, token):
    return "vlms :: {} :: {} :: {}".format(secret, token, userid)


def sha256_hash(data):
    digester = hashlib.sha256()
    digester.update(data.encode("utf-8"))
    msg_hash = digester.digest()
    print("Message: {}".format(data))
    print("Message Hash: {}".format(base64.b64encode(msg_hash)))
    return msg_hash


def sign(secret, userid, token, pvtkey):
    msg_hash = sha256_hash(payload(secret, userid, token))
    signer = EVP.PKey(md="sha256")
    signer.assign_rsa(pvtkey, capture=False)
    signer.sign_init()
    signer.sign_update(msg_hash)
    return signer.sign_final()


def verify(secret, userid, token, signature, pubkey):
    msg_hash = sha256_hash(payload(secret, userid, token))
    verifier = EVP.PKey(md="sha256")
    verifier.assign_rsa(pubkey, capture=False)
    verifier.verify_init()
    verifier.verify_update(msg_hash)
    return verifier.verify_final(signature)


def load_signature(sigfile):
    with open(sigfile, "rb") as sf:
        return sf.read()


if __name__ == "__main__":
    userid = "namesakes"
    token = "5600f6c2-24c1-47b7-9cc8-53665e5a327a"
    secret = "This is a secret"

    private_key = RSA.load_key("private.nopass.pem")
    public_key = RSA.load_pub_key("public.pem")

    signature = sign(secret, userid, token, private_key)
    print("Signature: {}".format(base64.b64encode(signature)))

    verify_status = verify(secret, userid, token, signature, public_key)
    print("Verification: {}".format(verify_status))


# openssl genpkey -algorithm RSA -aes256 -out private.pem
# openssl rsa -in private.pem -pubout -outform PEM -out public.pem
# openssl rsa -in private.pem -out private.nopass.pem
# echo -n "some secret" | openssl dgst -sha256 -sign private.nopass.pem  > helloTest
# echo -n "some secret" | openssl dgst -sha256 -verify public.pem -signature helloTest
# openssl base64 -in helloTest -out sha256.b64
