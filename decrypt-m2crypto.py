# pip install M2Crypto

import base64, hashlib, os
from M2Crypto import EVP

# The file encrypted in java could be decrypted here
# The file encrypted here could be decrypted in java

def load_secrets(fname):
  with open(fname, "r") as f:
    pwd = f.readline()[2:-1]
    salt = f.readline()[2:-1]
  return pwd, salt

pwd, salt = load_secrets("keysecrets.txt")
print(pwd)
print(salt)

key = hashlib.pbkdf2_hmac("sha256", pwd.encode(), base64.b64decode(salt), 100000, dklen=32)
cbc_cipher_mode = "aes_256_cbc"
ctr_cipher_mode = "aes_256_ctr"

def encrypt_file(cipher_mode, key, infile, outfile=None, chunksize=64*1024):
  if not outfile:
    outfile = "{}.enc".format(infile)

  iv = os.urandom(16)
  cipher = EVP.Cipher(cipher_mode, key=key, iv=iv, op=1)

  with open(infile, "rb") as inf:
    with open(outfile, "wb") as outf:
      outf.write(iv)
      while True:
        chunk = inf.read(chunksize)
        if len(chunk) == 0:
          break
        outf.write(cipher.update(chunk))
      outf.write(cipher.final())

def decrypt_file(cipher_mode, key, in_filename, out_filename=None, chunksize=64*1024):
    if not out_filename:
        out_filename = "{}.dec".format(os.path.splitext(in_filename)[0])

    with open(in_filename, 'rb') as infile:
        iv = infile.read(16)
        cipher = EVP.Cipher(cipher_mode, key=key, iv=iv, op=0)

        with open(out_filename, 'wb') as outfile:
            while True:
              chunk = infile.read(chunksize)
              if len(chunk) == 0:
                  break
              outfile.write(cipher.update(chunk))
            outfile.write(cipher.final())

# encrypt_file(cbc_cipher_mode, key, "sample.xlsx")
# decrypt_file(cbc_cipher_mode, key, "sample.xlsx.enc")

encrypt_file(cbc_cipher_mode, key, "file1.txt")
decrypt_file(cbc_cipher_mode, key, "file1.txt.enc")

encrypt_file(ctr_cipher_mode, key, "file4.txt")
decrypt_file(ctr_cipher_mode, key, "file4.txt.enc")
