# pip install cryptography
import base64, hashlib, os
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers import (
    Cipher, algorithms, modes
)

def load_secrets(fname):
  with open(fname, "r") as f:
    pwd = f.readline()[2:-1]
    salt = f.readline()[2:-1]
  return pwd, salt

pwd, salt = load_secrets("keysecrets.txt")
print(pwd)
print(salt)

KEY = hashlib.pbkdf2_hmac("sha256", pwd.encode(), base64.b64decode(salt), 100000, dklen=32)

# KEY = b"AES256Key-32Characters1234567890"
# KEY = b"1234567890abcdef"
associated_data = b"associated but unencrypted"

def _cipher(key):
  return Cipher(
    algorithms.AES(key),
    None,
    backend=default_backend()
  )

def gcm_encrypt_cipher(key, iv):
  c = _cipher(key)
  c.mode = modes.GCM(iv)
  return c.encryptor()

def gcm_decrypt_cipher(key, iv, tag):
  c = _cipher(key)
  c.mode = modes.GCM(iv, tag)
  return c.decryptor()

def encrypt_file(key, infile, associated_data, outfile=None, chunksize=64*1024):
  if not outfile:
    outfile = "{}.enc".format(infile)

  iv = os.urandom(12)
  cipher = gcm_encrypt_cipher(key, iv)
  cipher.authenticate_additional_data(associated_data)

  with open(infile, "rb") as inf:
    with open(outfile, "wb") as outf:
      outf.write(iv)
      while True:
        chunk = inf.read(chunksize)
        if len(chunk) == 0:
          break
        outf.write(cipher.update(chunk))
      outf.write(cipher.finalize())

  return cipher.tag

def decrypt_file(key, in_filename, associated_data, tag, out_filename=None, chunksize=64*1024):
    if not out_filename:
        out_filename = "{}.dec".format(os.path.splitext(in_filename)[0])

    with open(in_filename, 'rb') as infile:
        iv = infile.read(12)
        cipher = gcm_decrypt_cipher(key, iv, tag)
        cipher.authenticate_additional_data(associated_data)

        with open(out_filename, 'wb') as outfile:
            while True:
              chunk = infile.read(chunksize)
              if len(chunk) == 0:
                  break
              outfile.write(cipher.update(chunk))
            outfile.write(cipher.finalize())


# encrypt_file(key, "sample.xlsx")
# decrypt_file(key, "sample.xlsx.enc")
tag = encrypt_file(KEY, "file1.txt", associated_data)
print(tag)
decrypt_file(KEY, "file1.txt.enc", associated_data, tag)
