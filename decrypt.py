# pip install pycryptodome

import base64, struct, os, random, hashlib
from Crypto.Cipher import AES
from Crypto.Protocol.KDF import PBKDF2

def load_secrets(fname):
  with open(fname, "r") as f:
    pwd = f.readline()[2:-1]
    salt = f.readline()[2:-1]
    iv = f.readline()[2:-1]
  return pwd, salt, iv

pwd, salt, iv = load_secrets("keysecrets.txt")
print(pwd)
print(salt)
print(iv)
print(len(base64.b64decode(iv)))

# The encrypted file could not be decrypted in java
# Exception in thread "main" javax.crypto.BadPaddingException: Given final block not properly padded. Such issues can arise if a bad key is used during decryption.

# The file encrypted in java was decrypted successfully but with lots of whitespaces in the end

key = hashlib.pbkdf2_hmac("sha256", pwd.encode(), base64.b64decode(salt), 100000, dklen=32)


def encrypt_file(key, infile, outfile=None, chunksize=64*1024):
  if not outfile:
    outfile = "{}.enc".format(infile)

  # iv = ''.join(chr(random.randint(0, 0xFF)) for i in range(16))
  iv1 = base64.b64decode(iv)
  encryptor = AES.new(key, AES.MODE_CBC, iv1)
  filesize = os.path.getsize(infile)

  with open(infile, "rb") as inf:
    with open(outfile, "wb") as outf:
      # outf.write(struct.pack('<Q', filesize))
      # outf.write(iv1)

      while True:
        chunk = inf.read(chunksize)
        if len(chunk) == 0:
          break
        elif len(chunk) % 16 != 0:
          chunk += b' ' * (16 - len(chunk) % 16)

        outf.write(encryptor.encrypt(chunk))

def decrypt_file(key, in_filename, out_filename=None, chunksize=24*1024):
    if not out_filename:
        out_filename = "{}.dec".format(os.path.splitext(in_filename)[0])

    with open(in_filename, 'rb') as infile:
        # origsize = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]
        # iv = infile.read(16)
        iv1 = base64.b64decode(iv)
        decryptor = AES.new(key, AES.MODE_CBC, iv1)

        with open(out_filename, 'wb') as outfile:
            while True:
                chunk = infile.read(chunksize)
                if len(chunk) == 0:
                    break
                outfile.write(decryptor.decrypt(chunk))

            # outfile.truncate(origsize)

# encrypt_file(key, "file1.txt")
decrypt_file(key, "file1.txt.enc")
