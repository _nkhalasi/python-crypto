# pip install cryptograhy

import base64, hashlib, os
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, padding
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

# The file encrypted here could not be decrypted in java
# Exception in thread "main" javax.crypto.BadPaddingException: Given final block not properly padded. Such issues can arise if a bad key is used during decryption.

# The file encrypted in java was decrypted successfully but with lots of whitespaces in the end

def load_secrets(fname):
  with open(fname, "r") as f:
    pwd = f.readline()[2:-1]
    salt = f.readline()[2:-1]
    iv = f.readline()[2:-1]
  return pwd, salt, iv

pwd, salt, iv = load_secrets("keysecrets.txt")
print(pwd)
print(salt)
print(iv)

key = hashlib.pbkdf2_hmac("sha256", pwd.encode(), base64.b64decode(salt), 100000, dklen=32)
cipher = Cipher(algorithms.AES(key), modes.CBC(base64.b64decode(iv)), backend=default_backend())
decryptor = cipher.decryptor()
encryptor = cipher.encryptor()

def encrypt_file(key, infile, outfile=None, chunksize=64*1024):
  if not outfile:
    outfile = "{}.enc".format(infile)

  with open(infile, "rb") as inf:
    with open(outfile, "wb") as outf:
      while True:
        chunk = inf.read(chunksize)
        if len(chunk) == 0:
          break
        elif len(chunk) % 16 != 0:
          chunk += b' ' * (16 - len(chunk) % 16)

        outf.write(encryptor.update(chunk))
      outf.write(encryptor.finalize())

def decrypt_file(key, in_filename, out_filename=None, chunksize=64*1024):
    if not out_filename:
      out_filename = "{}.dec".format(os.path.splitext(in_filename)[0])

    with open(in_filename, 'rb') as infile:
      with open(out_filename, 'wb') as outfile:
        while True:
          chunk = infile.read(chunksize)
          if len(chunk) == 0:
              break
          outfile.write(decryptor.update(chunk))
        outfile.write(decryptor.finalize())

# encrypt_file(key, "file1.txt")
decrypt_file(key, "file1.txt.enc")
