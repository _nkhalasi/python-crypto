# pip install pyaes pycrypto

#this code does not work

import base64
import pyaes, pbkdf2, binascii, secrets

def load_secrets(fname):
  with open(fname, "r") as f:
    pwd = f.readline()[2:-1]
    salt = f.readline()[2:-1]
    iv = f.readline()[2:-1]
  return pwd, salt, iv

pwd, salt, iv = load_secrets("keysecrets.txt")
print(pwd)
print(salt)
print(iv)

key = pbkdf2.PBKDF2(pwd, base64.b64decode(salt)).read(32)
mode = pyaes.AESModeOfOperationCBC(key, base64.b64decode(iv))

with open("file1.txt", "rb") as inf:
  with open("file1.txt.enc", "wb") as outf:
    pyaes.encrypt_stream(mode, inf, outf)

with open("file1.txt.enc", "rb") as inf:
  with open("file1.txt.dec", "wb") as outf:
    pyaes.decrypt_stream(mode, inf, outf)
